import 'package:get/get.dart';
import 'package:testaverin/core/app_routes.dart';
import 'package:testaverin/pages/edit_page.dart';
import 'package:testaverin/pages/form_page.dart';
import 'package:testaverin/pages/view_page.dart';

abstract class AppPages {
  static final pages = [
    GetPage(name: Routes.form, page: () => FormPage()),
    GetPage(name: Routes.view, page: () => ViewPage()),
    GetPage(name: Routes.edit, page: () => EditPage()),
  ];
}
