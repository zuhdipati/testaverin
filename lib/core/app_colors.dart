import 'package:flutter/material.dart';

class ColorsApp {
  static const secondaryColor = Color(0xffE4E4E4);
  static const primaryColor = Color(0xffF6F6F6);
  static const white = Colors.white;
  static const black = Colors.black;
  static const grey = Colors.grey;
}
