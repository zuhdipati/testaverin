class Provinsi {
  int? code;
  List<ListItem>? list;

  Provinsi({this.code, this.list});

  Provinsi.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    if (json['list'] != null) {
      list = List<ListItem>.from(json['list'].map((x) => ListItem.fromJson(x)));
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    if (list != null) {
      data['list'] = list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListItem {
  String? kode;
  String? nama;

  ListItem({this.kode, this.nama});

  ListItem.fromJson(Map<String, dynamic> json) {
    kode = json['kode'];
    nama = json['nama'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['kode'] = kode;
    data['nama'] = nama;
    return data;
  }
}
