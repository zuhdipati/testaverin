import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testaverin/core/app_colors.dart';
import 'package:testaverin/services/api_service.dart';

class KecamatanController extends GetxController {
  final Service service = Get.find();
  var kecamatanTextFieldController = TextEditingController().obs;

  void showKecamatanBottomSheet(BuildContext context) {
    Get.bottomSheet(
      Container(
        color: ColorsApp.white,
        height: Get.height / 3,
        child: Obx(
          () => ListView.builder(
              itemCount: service.kecamatan.value.list!.length,
              itemBuilder: (context, index) {
                final kecamatan = service.kecamatan.value.list![index];
                return ListTile(
                  title: Text(kecamatan.nama!),
                  onTap: () {
                    kecamatanTextFieldController.value.text = kecamatan.nama!;
                    Get.back(result: kecamatan);
                  },
                );
              }),
        ),
      ),
    );
  }

  @override
  void onInit() {
    super.onInit();
    showKecamatanBottomSheet;
  }
}
