import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testaverin/controllers/kota_controller.dart';
import 'package:testaverin/controllers/provinsi_controller.dart';
import 'package:testaverin/core/app_colors.dart';
import 'package:testaverin/services/api_service.dart';

class Controller extends GetxController {
  final Service service = Get.find();
  var kotaController = Get.put(KotaController());
  var provinsiController = Get.put(ProvinsiController());

  var namaTextFieldController = TextEditingController().obs;
  var emailTextFieldController = TextEditingController().obs;
  var nomorTextFieldController = TextEditingController().obs;
  var tempatLahirTextFieldController = TextEditingController().obs;
  var tanggalLahirTextFieldController = TextEditingController().obs;
  var alamatTextFieldController = TextEditingController().obs;

  List form = [
    "Name",
    "Email",
    "No HP",
    "Tempat Lahir",
    "Tanggal Lahir",
    "Alamat",
    "Provinsi",
    "Kota",
    "Kecamatan",
  ];

  Color checkColor(String state) {
    Color colorSelect = ColorsApp.primaryColor;
    if (state == "Kecamatan") {
      if (kotaController.kotaTextFieldController.value.text.isEmpty) {
        colorSelect = ColorsApp.grey;
      }
    } else if (state == "Kota") {
      if (provinsiController.provinsiTextFieldController.value.text.isEmpty) {
        colorSelect = ColorsApp.grey;
      }
    }
    return colorSelect;
  }
}
