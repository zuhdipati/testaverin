import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testaverin/controllers/kecamatan_controller.dart';
import 'package:testaverin/core/app_colors.dart';
// import 'package:testaverin/models/kecamatan_moel.dart';
import 'package:testaverin/services/api_service.dart';

class KotaController extends GetxController {
  final Service service = Get.find();
  var kecamatanController = Get.put(KecamatanController());
  var kotaTextFieldController = TextEditingController().obs;

  void showKotaBottomSheet(BuildContext context) {
    Get.bottomSheet(
      Container(
        color: ColorsApp.white,
        height: Get.height / 3,
        child: Obx(
          () => ListView.builder(
              itemCount: service.kotaModel.value.list!.length,
              itemBuilder: (context, index) {
                final kota = service.kotaModel.value.list![index];
                return ListTile(
                  title: Text(kota.nama!),
                  onTap: () {
                    kotaTextFieldController.value.text = kota.nama!;
                    kecamatanController.kecamatanTextFieldController.value
                        .clear();
                    kotaTextFieldController.refresh();
                    // service.kecamatan.value = Kecamatan();
                    service.getKecamatan(kota.kode!);
                    Get.back(result: kota);
                  },
                );
              }),
        ),
      ),
    );
  }

  @override
  void onInit() {
    super.onInit();
    showKotaBottomSheet;
  }
}
