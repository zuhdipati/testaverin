import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testaverin/controllers/kecamatan_controller.dart';
import 'package:testaverin/controllers/kota_controller.dart';
import 'package:testaverin/core/app_colors.dart';
// import 'package:testaverin/models/kota_model.dart';
import 'package:testaverin/services/api_service.dart';

class ProvinsiController extends GetxController {
  final Service service = Get.find();
  var provinsiTextFieldController = TextEditingController().obs;
  var kotaController = Get.put(KotaController());
  var kecamatanController = Get.put(KecamatanController());

  void showProvinsiBottomSheet(BuildContext context) {
    Get.bottomSheet(
      Container(
        color: ColorsApp.white,
        height: Get.height / 2.5,
        child: Obx(
          () => ListView.builder(
              itemCount: service.provinsi.value.list!.length,
              itemBuilder: (context, index) {
                final provinsi = service.provinsi.value.list![index];
                return ListTile(
                  title: Text(provinsi.nama!),
                  onTap: () {
                    provinsiTextFieldController.value.text = provinsi.nama!;
                    kotaController.kotaTextFieldController.value.clear();
                    kecamatanController.kecamatanTextFieldController.value
                        .clear();
                    // service.kotaModel.value = Kota();
                    provinsiTextFieldController.refresh();
                    service.getKota(provinsi.kode!);
                    Get.back(result: provinsi);
                  },
                );
              }),
        ),
      ),
    );
  }

  @override
  void onInit() {
    super.onInit();
    showProvinsiBottomSheet;
  }
}
