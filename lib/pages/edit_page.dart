// ignore_for_file: unused_local_variable, unused_import

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:testaverin/controllers/controller.dart';
import 'package:testaverin/controllers/kecamatan_controller.dart';
import 'package:testaverin/controllers/kota_controller.dart';
import 'package:testaverin/controllers/provinsi_controller.dart';
import 'package:testaverin/core/app_colors.dart';
import 'package:testaverin/core/app_routes.dart';
import 'package:testaverin/pages/view_page.dart';
import 'package:testaverin/services/api_service.dart';
import 'package:testaverin/services/constant.dart';

class EditPage extends GetView {
  EditPage({super.key});
  final box = GetStorage();

  @override
  Widget build(BuildContext context) {
    var service = Get.put(Service());
    var controller = Get.put(Controller());
    var provinsiController = Get.put(ProvinsiController());
    var kotaController = Get.put(KotaController());
    var kecamatanController = Get.put(KecamatanController());

    return Scaffold(
      backgroundColor: ColorsApp.white,
      appBar: AppBar(
        backgroundColor: ColorsApp.white,
        elevation: 0,
        title: const Text(
          "Edit Data Diri",
          style: TextStyle(color: ColorsApp.black),
        ),
        centerTitle: true,
      ),
      body: ListView(
        shrinkWrap: true,
        children: [
          CircleAvatar(
            radius: Get.height / 13,
            backgroundColor: ColorsApp.primaryColor,
            child: const Text(
              "Edit",
              style: TextStyle(color: ColorsApp.grey),
            ),
          ),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: controller.form.length,
            itemBuilder: (context, index) {
              if (index <= 5) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: TextField(
                    controller: controller.form[index] == "Name"
                        ? controller.namaTextFieldController.value
                        : controller.form[index] == "Email"
                            ? controller.emailTextFieldController.value
                            : controller.form[index] == "No HP"
                                ? controller.nomorTextFieldController.value
                                : controller.form[index] == "Tempat Lahir"
                                    ? controller
                                        .tempatLahirTextFieldController.value
                                    : controller.form[index] == "Tanggal Lahir"
                                        ? controller
                                            .tanggalLahirTextFieldController
                                            .value
                                        : controller.form[index] == "Alamat"
                                            ? controller
                                                .alamatTextFieldController.value
                                            : null,
                    keyboardType: controller.form[index] == "Tanggal Lahir"
                        ? TextInputType.emailAddress
                        : TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: ColorsApp.primaryColor,
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide.none),
                      hintText: controller.form[index],
                    ),
                  ),
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: InkWell(
                    onTap: () {
                      if (controller.form[index] == "Provinsi") {
                        provinsiController.showProvinsiBottomSheet(context);
                      } else if (controller.form[index] == "Kota") {
                        kotaController.showKotaBottomSheet(context);
                      } else if (controller.form[index] == "Kecamatan") {
                        kecamatanController.showKecamatanBottomSheet(context);
                      }
                    },
                    child: TextField(
                      enabled: false,
                      controller: controller.form[index] == "Provinsi"
                          ? provinsiController.provinsiTextFieldController.value
                          : controller.form[index] == "Kota"
                              ? kotaController.kotaTextFieldController.value
                              : controller.form[index] == "Kecamatan"
                                  ? kecamatanController
                                      .kecamatanTextFieldController.value
                                  : null,
                      decoration: InputDecoration(
                        fillColor: ColorsApp.primaryColor,
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none),
                        hintText: controller.form[index],
                        suffixIcon: const Icon(Icons.arrow_drop_down_outlined),
                      ),
                    ),
                  ),
                );
              }
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 80),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                box.write(
                    'name', controller.namaTextFieldController.value.text);
                box.write(
                    'email', controller.emailTextFieldController.value.text);
                box.write(
                    'nomor', controller.nomorTextFieldController.value.text);
                box.write('tempat',
                    controller.tempatLahirTextFieldController.value.text);
                box.write('tanggal',
                    controller.tanggalLahirTextFieldController.value.text);
                box.write(
                    'alamat', controller.alamatTextFieldController.value.text);
                box.write('provinsi',
                    provinsiController.provinsiTextFieldController.value.text);
                box.write(
                    'kota', kotaController.kotaTextFieldController.value.text);
                box.write(
                    'kecamatan',
                    kecamatanController
                        .kecamatanTextFieldController.value.text);
                // Get.toNamed(Routes.view);
                Get.to(ViewPage());
              },
              child: const Text('Simpan'),
            ),
          )
        ],
      ),
    );
  }
}
