import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:testaverin/controllers/controller.dart';
import 'package:testaverin/controllers/kecamatan_controller.dart';
import 'package:testaverin/controllers/kota_controller.dart';
import 'package:testaverin/controllers/provinsi_controller.dart';
import 'package:testaverin/core/app_colors.dart';
import 'package:testaverin/pages/edit_page.dart';

class ViewPage extends GetView {
  ViewPage({super.key});
  final box = GetStorage();

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(Controller());
    var provinsiController = Get.put(ProvinsiController());
    var kotaController = Get.put(KotaController());
    var kecamatanController = Get.put(KecamatanController());

    return Scaffold(
      backgroundColor: ColorsApp.white,
      appBar: AppBar(
        backgroundColor: ColorsApp.white,
        elevation: 0,
        title: const Text(
          "View Data Diri",
          style: TextStyle(color: ColorsApp.black),
        ),
        centerTitle: true,
      ),
      body: ListView(
        shrinkWrap: true,
        children: [
          CircleAvatar(
            radius: Get.height / 13,
            backgroundColor: ColorsApp.primaryColor,
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorsApp.primaryColor,
            ),
            child: ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: controller.form.length,
              itemBuilder: (context, index) {
                String dataLabel = controller.form[index];
                String? savedData;

                if (dataLabel == "Provinsi") {
                  savedData = box.read('provinsi');
                } else if (dataLabel == "Kota") {
                  savedData = box.read('kota');
                } else if (dataLabel == "Kecamatan") {
                  savedData = box.read('kecamatan');
                } else if (dataLabel == "Name") {
                  savedData = box.read('name');
                } else if (dataLabel == "Email") {
                  savedData = box.read('email');
                } else if (dataLabel == "No HP") {
                  savedData = box.read('nomor');
                } else if (dataLabel == "Tempat Lahir") {
                  savedData = box.read('tempat');
                } else if (dataLabel == "Tanggal Lahir") {
                  savedData = box.read('tanggal');
                } else if (dataLabel == "Alamat") {
                  savedData = box.read('alamat');
                }
                return Container(
                  margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: ColorsApp.secondaryColor,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 12, 18, 12),
                        child: Text("${controller.form[index]} : $savedData"),
                      )
                    ],
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 80),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                box.write(
                    'name', controller.namaTextFieldController.value.text);
                box.write(
                    'email', controller.emailTextFieldController.value.text);
                box.write(
                    'nomor', controller.nomorTextFieldController.value.text);
                box.write('tempat',
                    controller.tempatLahirTextFieldController.value.text);
                box.write('tanggal',
                    controller.tanggalLahirTextFieldController.value.text);
                box.write(
                    'alamat', controller.alamatTextFieldController.value.text);
                box.write('provinsi',
                    provinsiController.provinsiTextFieldController.value.text);
                box.write(
                    'kota', kotaController.kotaTextFieldController.value.text);
                box.write(
                    'kecamatan',
                    kecamatanController
                        .kecamatanTextFieldController.value.text);
                // Get.toNamed(Routes.view);
                Get.to(EditPage());
              },
              child: const Text('Edit'),
            ),
          )
        ],
      ),
    );
  }
}
