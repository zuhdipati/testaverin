const String apikey =
    'd40f936a3c8b8d077aae49fe8046c647822f4d692891de690ea4cfb24fa27d97';
const String urlbase = 'https://a-dokter.id/api/v1/get-';

const String token = '${urlbase}token.php';
const String provinsi = '${urlbase}provinsi.php';
const String agama = '${urlbase}agama.php';
const String kota = '${urlbase}kota.php';
const String kecamatan = '${urlbase}kecamatan.php';
const String pekerjaan = '${urlbase}pekerjaan.php';
const String kelurahan = '${urlbase}kelurahan.php';
const String goldarah = '${urlbase}gol-darah.php';
