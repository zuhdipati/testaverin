// ignore_for_file: avoid_print

import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:testaverin/models/kecamatan_moel.dart';
import 'package:testaverin/models/kota_model.dart';
import 'package:testaverin/models/provinsi_model.dart';
import 'dart:convert' as convert;

import 'constant.dart' as constant;

class Service extends GetxController {
  var token = ''.obs;
  var provinsi = Provinsi().obs;
  var kotaModel = Kota().obs;
  var kecamatan = Kecamatan().obs;

  Future getToken() async {
    var url = Uri.parse(constant.token);
    var body = jsonEncode({"KeyCode": constant.apikey});

    var response = await http.post(url, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      token.value = jsonResponse['token'];
      print('Token: $token');
    } else {
      print('Error: ${response.statusCode}');
    }
  }

  Future getProvinsi() async {
    var url = Uri.parse(constant.provinsi);
    var headers = {'X-Api-Token': token.value, 'token': ''};

    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      final jsonData = convert.jsonDecode(response.body);
      var data = Provinsi.fromJson(jsonData);
      provinsi.value = data;

      for (var i = 0; i < provinsi.value.list!.length; i++) {
        print(provinsi.value.list![i].nama);
      }
    } else {
      print('Error: ${response.statusCode}');
    }
  }

  Future getKota(String idProvinsiSelected) async {
    var url = Uri.parse(constant.kota);
    var headers = {'X-Api-Token': token.value, 'token': ''};
    var body = {"id_provinsi": idProvinsiSelected};

    var response = await http.post(url, headers: headers, body: body);
    if (response.statusCode == 200) {
      final jsonData = convert.jsonDecode(response.body);
      var data = Kota.fromJson(jsonData);
      kotaModel.value = data;

      for (var i = 0; i < kotaModel.value.list!.length; i++) {
        print(kotaModel.value.list![i].nama);
      }
    } else {
      print('Error: ${response.statusCode}');
    }
  }

  Future getKecamatan(String idKotaSelected) async {
    var url = Uri.parse(constant.kecamatan);
    var headers = {'X-Api-Token': token.value, 'token': ''};
    var body = {"id_kota": idKotaSelected};

    var response = await http.post(url, headers: headers, body: body);
    if (response.statusCode == 200) {
      final jsonData = convert.jsonDecode(response.body);
      var data = Kecamatan.fromJson(jsonData);
      kecamatan.value = data;

      for (var i = 0; i < kecamatan.value.list!.length; i++) {
        print(kecamatan.value.list![i].nama);
      }
    } else {
      print('Error: ${response.statusCode}');
    }
  }

  void initialize() async {
    await getToken();
    await getProvinsi();
    // await getKota();
    // await getKecamatan();
  }

  @override
  void onInit() {
    super.onInit();
    initialize();
  }
}
